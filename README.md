## TODOs:
- zram
- plymouth
- add example disk partition layouts, with vg and lvs added, with luks device opened

## UEFI Encrypted / Non-Encrypted Installation

### After Boot: Set up Environment
Start `tmux`, set a bigger tty font, get the local IPv4 address, set the root password in the live environment, and make sure `sshd` is started:

   ```bash
   tmux
   
   setfont ter-132n

   ip r

   passwd
   
   systemctl start sshd
   ```
   
For automation, to query external IP address:
   
   ```ip -j r | jq -r '.[] | select(.dst == "default") | .prefsrc'```


If you have to get through a NAT (for example virtualbox VM without port forwards), set up a (-R)emote SSH tunnel (AKA ssh port-forward) on the vm to the host so you can ssh to the archiso vm


Run this first on the guest to set up the tunnel:

    ssh -fNR 10022:localhost:22 myuser@10.0.2.2

And then this on the host to use the tunnel:

    ssh -p 10022 root@localhost


10.0.2.2 is the hypervisor host machine, the one running virtualbox.

The rest can be done from your main pc. Use the IP address of the target install system here, or the ssh tunnel commands from above:

    ssh root@192.168.1.185
    
Attach to the running tmux session and inspect your current disk and partitioning layout:

    tmux attach	
    lsblk


Do one of the following two sections, install with or without encryption:
### Option 1: Encryption with LVM on LUKS

Open up your target install storage AKA block device with cfdisk for easy TUI menu partitioning. which is probably one of the following commands if you only have one disk:

To keep things simple, we assume a single nvme drive installed in your target system. In that case its name will be `/dev/nvme0n1`, and its partitions that you will create in the next step with cfdisk will be `/dev/nvme0n1p1`, `/dev/nvme0n1p2`, `/dev/nvme0n1p3`.  
(the nvme drives are zero indexed, the partitions are not)

But you might get different names depending on the type and amount of storage devices you have. Typical device types you can encounter are: `sda`, `vda`, `nvme0n1`, `mmcblk0`.

    cfdisk /dev/nvme0n1
    
- Create a 512 MiB [EFI System] partition (ESP)  
- Create a 1 GiB   [Linux filesystem] partition
- Create a partition with the remaining space [Linux LVM]

Format the encrypted lvm partition:

    cryptsetup luksFormat /dev/nvme0n1p3
    YES
    <passphrase>
    <passphrase>

Decrypt the just created luks volume:

    cryptsetup open --type luks /dev/nvme0n1p3 archlinux-crypt
    <passphrase>

Create the phypical volume, the volume group, and two logical volumes. Adjust root lv size as needed.

    pvcreate --dataalignment 1m /dev/mapper/archlinux-crypt
    vgcreate vg_archlinux /dev/mapper/archlinux-crypt
    lvcreate -L 200GB vg_archlinux -n lv_root
    lvcreate -l 100%FREE vg_archlinux -n lv_home


archlinux-crypt, vg_archlinux, lv_root, and lv_home are arbitrary set but descriptively chosen names here.  
I recommend to not use hyphens in lv or vg names: https://access.redhat.com/solutions/2267481  


make sure device mapper is enabled and active the volume group:

    modprobe dm_mod
    vgscan
    vgchange -ay
    
create the filesystems on the partitions, create mount directories, and mount the filesystems:

    mkfs.ext4 /dev/vg_archlinux/lv_root
    mount /dev/vg_archlinux/lv_root /mnt
    mkdir /mnt/boot
    mount /dev/nvme0n1p2 /mnt/boot
    mkfs.ext4 /dev/vg_archlinux/lv_home
    mkdir /mnt/home
    mount /dev/vg_archlinux/lv_home /mnt/home
    mkdir /mnt/etc
    genfstab -U -p /mnt >> /mnt/etc/fstab




or without encryption:

### Option 2: partitioning without encryption, without lvm

- Create a 512MiB UEFI System Partition (ESP)
- Create a root partition with the remaining space
    

    mkfs.vfat /dev/nvme0n1p1 -F32 # ESP
    mkfs.ext4 /dev/nvme0n1p2 # root
    mount /dev/nvme0n1p2 /mnt
    mkdir /mnt/etc
    genfstab -U -p /mnt >> /mnt/etc/fstab
    mkdir -p /mnt/boot/EFI
    mount /dev/nvme0n1p1 /mnt/boot/EFI


in case the downloaded arch iso file has gotten too old you have to install and init the latest pacman keys
pacman -S archlinux-keyring
pacman-key --init

pacstrap /mnt base base-devel linux linux-firmware gnome openssh gvim grub efibootmgr dosfstools mtools os-prober openssh networkmanager wpa_supplicant wireless_tools dialog lvm2 git

intel-ucode / amd-ucode / (not needed in VM)

arch-chroot /mnt

groups:   
vboxusers docker sambausers libvirt mpr video lp kvm wheel

systemctl enable NetworkManager
passwd

vim /etc/locale.gen, uncomment:
en_US.UTF-8 UTF-8
nl_NL.UTF-8 UTF-8
locale-gen
localectl set-locale LANG=en_US.UTF-8
useradd -m -G wheel mpr
passwd mpr

visudo, or:
echo -e "%wheel ALL=(ALL) ALL" > /etc/sudoers.d/99_wheel

su mpr
cd

git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

control-D (exit mpr, become root again)

grub-install --target=x86_64-efi --bootloader-id=archgrub --recheck

vim /etc/default/grub
GRUB_DEFAULT=saved
GRUB_SAVEDEFAULT=true
in case of encrypted install, specify the logical :
GRUB_CMDLINE_LINUX_DEFAULT="cryptdevice=/dev/nvme0n1p3:luksdev:allow-discards log"
                                            /sda3
grub-mkconfig -o /boot/grub/grub.cfg

vim /etc/mkinitcpio.conf
between block and filesystems, add: encrypt lvm2:
HOOKS=(base udev autodetect modconf block encrypt lvm2 filesystems keyboard fsck)
sed -i 's/block filesystems/block encrypt lvm2 filesystems/' /etc/mkinitcpio.conf
mkinitcpio -p linux


create and enable a swapfile of amount of MiB:
1024 2048 4096 8192 16384 
dd if=/dev/zero of=/swapfile bs=1M count=16384 status=progress
chmod 600 /swapfile
mkswap /swapfile

ln -sf /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime


systemctl enable systemd-timesyncd

#hostnamectl set-hostname <hostname>
echo <hostname> > /etc/hostname

systemctl enable gdm

if booting with other operating system, add entries to boot those to grub:
echo "GRUB_DISABLE_OS_PROBER=false" >> /etc/default/grub && grub-mkconfig -o /boot/grub/grub.cfg 
don't forget to set windows to interpret RTC as UTC time in that case
on windows: google for rtc_in_utc.reg

/etc/makepkg.conf
change MAKEFLAGS to "-j8" or according to amount of cpu vcores (= cores * [2 if HT, else 1])

gdm set xorg as default instead of wayland
/etc/gdm/custom.conf
uncomment:
# Uncomment the line below to force the login screen to use Xorg
WaylandEnable=false

reboot, locale can't be done in chroot. find a better way to do this instead of having to do this after reboot
vim /etc/locale.gen, uncomment:
en_US.UTF-8 UTF-8
nl_NL.UTF-8 UTF-8
locale-gen
localectl set-locale LANG=en_US.UTF-8

LC_NUMERIC=nl_NL.UTF-8
LC_TIME=nl_NL.UTF-8
LC_MONETARY=nl_NL.UTF-8
LC_PAPER=nl_NL.UTF-8
LC_MEASUREMENT=nl_NL.UTF-8

gnome set language before being able to open terminal

dark theme qt applications:
Edit /etc/environment as root
add the line QT_QPA_PLATFORMTHEME=qt5ct:qt6ct
https://wiki.archlinux.org/title/Qt

or kvantum?

/etc/pacman.conf
uncomment
Color
[multilib]
Include = /etc/pacman.d/mirrorlist

migrating other installation:
https://wiki.archlinux.org/title/Migrate_installation_to_new_hardware#List_of_installed_packages
on source system:
pacman -Qqen > pkglist.txt
pacman -Qqem > pkglist_aur.txt
dconf dump / > full-dconf-backup

scp pkglist.txt pkglist_aur.txt full-dconf-backup newhost:/home/mpr/

on new install:
pacman -S --needed - < pkglist.txt
pacman -S --needed - < pkglist_aur.txt
dconf load / < full-dconf-backup

rsync -aXSAUH --progress --preallocate /home/mpr/ riddle:/home/mpr/

The packages have to be installed first for the groups to exist, but the groups can already be created beforehand:
usermod -aG vboxusers,audio,wireshark,docker,video <username>



allow the regular user to toggle the bluetooth on/off state in gnome settings
might also have to install pipewire if it hasn't replaced pulseaudio as the default by now. pipewire pipewire-pulse
to be able to toggle bluetooth in gnome, install gnome-bluetooth-3.0, not gnome-bluetooth (legacy)

#TODO testing: two configuring files, logging out and back in or even rebooting, toggling bluetooth in gnome


sudo vim /etc/polkit-1/rules.d/90-bluetooth.rules
polkit.addRule(function(action, subject) {
    if (action.id == "org.freedesktop.systemd1.manage-units" &&
        action.lookup("unit") == "bluetooth.service" &&
        subject.isInGroup("wheel")) {
        return polkit.Result.YES;
    }
})


sudo vim /usr/share/dbus-1/system.d/bluetooth.conf
<!-- This configuration file specifies the required security policies
<!-- This configuration file specifies the required security policies
     for Bluetooth core daemon to work. -->

<!DOCTYPE busconfig PUBLIC "-//freedesktop//DTD D-BUS Bus Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
<busconfig>

  <!-- ../system.conf have denied everything, so we just punch some holes -->

  <policy user="root">
    <allow own="org.bluez"/>
    <allow send_destination="org.bluez"/>
    <allow send_interface="org.bluez.AdvertisementMonitor1"/>
    <allow send_interface="org.bluez.Agent1"/>
    <allow send_interface="org.bluez.MediaEndpoint1"/>
    <allow send_interface="org.bluez.MediaPlayer1"/>
    <allow send_interface="org.bluez.Profile1"/>
    <allow send_interface="org.bluez.GattCharacteristic1"/>
    <allow send_interface="org.bluez.GattDescriptor1"/>
    <allow send_interface="org.bluez.LEAdvertisement1"/>
    <allow send_interface="org.freedesktop.DBus.ObjectManager"/>
    <allow send_interface="org.freedesktop.DBus.Properties"/>
    <allow send_interface="org.mpris.MediaPlayer2.Player"/>
  </policy>

  <policy context="default">
    <allow send_destination="org.bluez"/>
  </policy>

</busconfig>

